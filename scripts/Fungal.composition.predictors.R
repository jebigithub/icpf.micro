#Make a statistical model exploring what structures fungal community composition

#Clear environment
rm(list = ls(all.names = TRUE))

#load packages
require(dplyr)
require(plyr)
require(vegan)
require(usethis)
require(phyloseq)
require(data.table)
require(ape)
require(ggplot2)
require(splitstackshape)
require(cowplot)
require(ggrepel)
require(indicspecies)
require(car)
require(ggrepel)
require(effectsize)

#load data paths
source('paths.R')

#Upload data files, add key modifications for matching
otu.table <- as.data.frame(fread(otu.file.string, sep = '\t'))
icp.dat <- readRDS(icp.date.with.age.string)
envt.mat <- read.csv(envt.file.string)
plot.dat <- readRDS(icp.plot.file.string)
ICP.tree.envt <- readRDS(individual.tree.growth.file.string)
fung.guild <- fread(funguild.file.string, sep = '\t')
taxonomy.table <- fread(taxonomy.reference.string, sep = '\t', header = FALSE)
soil.prf <- read.csv(soil.prf.data.string, sep = ';')
soil.prf$country_plot <- paste(soil.prf$code_country, soil.prf$code_plot, sep = '_')
fung.guild <- fread(funguild.file.string, sep = '\t')
soil.grids.data <- read.csv(soil.grids.data.string)
fungi.dat <- readRDS(community.alpha.beta.processed.string)
soil.ph.lab.meas <- read.csv(soil.ph.measurements.string) #Add the lab measured soil ph values
som.data <- read.csv(soil.org.matter.data.string, sep = ';')
fleck.data <- read.csv(fleck.soil.data.string)
fleck.data$country_plot <- paste(fleck.data$code_country, fleck.data$code_plot, sep = '_')
som.data.split <- split(som.data, som.data$code_layer)
som.data.split$M01$country_plot <- paste(som.data.split$M01$code_country, som.data.split$M01$code_plot, sep = '_')
som.data$country_plot <- paste(som.data$code_country, som.data$code_plot, sep = '_')
som.db.depth <- split(som.data, som.data$code_layer)
fleck.data.depth <- split(fleck.data, fleck.data$code_layer)


##########################################################################################
##############################Building the fungal OTU table for analysis##################
##########################################################################################

#Now curate the raw otu table to remove files from the greenhouse experiment
otu.tab2 <- otu.table
row.names(otu.tab2) <- otu.tab2$`#OTU ID`
otu.tab2$`#OTU ID` <- NULL

#Pre-treatment greenhouse samples
otu.tab2$S_364 <- NULL
otu.tab2$S_365<- NULL
otu.tab2$S_366<- NULL
otu.tab2$S_367<- NULL
otu.tab2$S_368<- NULL
otu.tab2$S_369<- NULL
otu.tab2$S_370<- NULL
otu.tab2$S_371<- NULL
otu.tab2$S_372<- NULL
otu.tab2$S_373<- NULL
otu.tab2$S_374<- NULL

#Amanita rubescens DNA barcode
otu.tab2$S_376<- NULL

#And the pesky IM and LTER plots that made there way in + the Blanks
otu.tab2$S_4<- NULL
otu.tab2$S_11<- NULL
otu.tab2$S_292<- NULL
otu.tab2$S_293<- NULL
otu.tab2$S_325<- NULL
otu.tab2$S_329<- NULL
otu.tab2$S_340<- NULL
otu.tab2$S_358<- NULL

otu.tab2$X2.S_29_2019<- NULL
otu.tab2$X2.S_41_2019<- NULL
otu.tab2$S_115_2019<- NULL
otu.tab2$X2.S_54_2019<- NULL
otu.tab2$X2.S_102_2019<- NULL
otu.tab2$X2.S_107_2019<- NULL
otu.tab2$X2.S_108_2019<- NULL
otu.tab2$X2.S_109_2019<- NULL
otu.tab2$X2.S_110_2019<- NULL
otu.tab2$X2.S_111_2019<- NULL
otu.tab2$X2.S_112_2019<- NULL


#Remove the german sites that are not part of ICP
otu.tab2$S_15_2019<- NULL
otu.tab2$S_22_2019<- NULL
otu.tab2$S_29_2019<- NULL
otu.tab2$S_36_2019<- NULL
otu.tab2$S_46_2019<- NULL
otu.tab2$S_56_2019<- NULL
otu.tab2$S_71_2019<- NULL
otu.tab2$X2.S_57_2019<- NULL

#Now lets look at column sums
sort(colSums(otu.tab2))
otu.tab2 <- data.frame(t(otu.tab2))

#Filter out those with <500 sequences as this could be too great a range between min and max depth
high.depth.otu<- subset(otu.tab2, rowSums(otu.tab2) > 500)

#Make environmental matrix 
phylo.envt.mat <- data.frame(Sample = row.names(high.depth.otu))
envt.mat$country_plot <- paste(envt.mat$Country, envt.mat$Plot, sep = '_')
phylo.envt.mat$country_plot <- envt.mat$country_plot[match(phylo.envt.mat$Sample, envt.mat$File_name)]
phylo.envt.mat$Soil.Horizon <- envt.mat$Soil.Horizon[match(phylo.envt.mat$Sample, envt.mat$File_name)]

#Rarefy to the lowest sequencing depth minus one
high.depth.otu <- rrarefy(as.matrix(high.depth.otu), (min(rowSums(high.depth.otu))-1))

#Make the data-frame proportional for relative abundances
high.depth.otu<- data.frame(prop.table(as.matrix(high.depth.otu), 1))

#Now split the OTU table by soil horizon
high.depth.otu$soil.horizon <- phylo.envt.mat$Soil.Horizon[match(row.names(high.depth.otu), phylo.envt.mat$Sample)]
high.otu.sp <- split(high.depth.otu, high.depth.otu$soil.horizon)

high.otu.sp$Min$soil.horizon <- NULL
high.otu.sp$Org$soil.horizon <- NULL


##########################################################################################
#######################Now build the table of environmental predictors####################
##########################################################################################

#First, compute SOC stocks

#M01 = 0-10 cm
#M12 = 10-20 cm
#M24 = 20- 40 cm
#M48 = 40-80 cm

soc.tog <- data.frame(country_plot= unique(c(fleck.data$country_plot, som.data$country_plot)))

M01 = data.frame(SOC = c(som.db.depth$M01$organic_carbon_total, fleck.data.depth$M01$organic_carbon_total), 
                 country_plot = c(som.db.depth$M01$country_plot, fleck.data.depth$M01$country_plot),
                 BD = c(som.db.depth$M01$bulk_density, fleck.data.depth$M01$bulk_density),
                 Coarse = c(som.db.depth$M01$coarse_fragment_vol, fleck.data.depth$M01$coarse_fragment_vol))
M12 = data.frame(SOC = c(som.db.depth$M12$organic_carbon_total, fleck.data.depth$M12$organic_carbon_total), 
                 country_plot = c(som.db.depth$M12$country_plot, fleck.data.depth$M12$country_plot),
                 BD = c(som.db.depth$M12$bulk_density, fleck.data.depth$M12$bulk_density),
                 Coarse = c(som.db.depth$M12$coarse_fragment_vol, fleck.data.depth$M12$coarse_fragment_vol))
M24 = data.frame(SOC = c(som.db.depth$M24$organic_carbon_total, fleck.data.depth$M24$organic_carbon_total), 
                 country_plot = c(som.db.depth$M24$country_plot, fleck.data.depth$M24$country_plot),
                 BD = c(som.db.depth$M24$bulk_density, fleck.data.depth$M24$bulk_density),
                 Coarse = c(som.db.depth$M24$coarse_fragment_vol, fleck.data.depth$M24$coarse_fragment_vol))
M48 = data.frame(SOC = c(som.db.depth$M48$organic_carbon_total, fleck.data.depth$M48$organic_carbon_total), 
                 country_plot = c(som.db.depth$M48$country_plot, fleck.data.depth$M48$country_plot),
                 BD = c(som.db.depth$M48$bulk_density, fleck.data.depth$M48$bulk_density),
                 Coarse = c(som.db.depth$M48$coarse_fragment_vol, fleck.data.depth$M48$coarse_fragment_vol))

#Add the organic horizon
OFH = data.frame(SOC = c(som.db.depth$OFH$organic_carbon_total, fleck.data.depth$OFH$organic_carbon_total), 
                 country_plot = c(som.db.depth$OFH$country_plot, fleck.data.depth$OFH$country_plot),
                 Weight = c(som.db.depth$OFH$organic_layer_weight, fleck.data.depth$OFH$organic_layer_weight))

OFH$OFH.SOC <- ((0.1*OFH$SOC)*OFH$Weight)*0.1 #% C x mass  (kg/m2) so multiply by 0.1 to get to t/ha
OFH$OFH.SOC <- replace(OFH$OFH.SOC, which(OFH$OFH.SOC < 0), NA)

M01.agg <- aggregate(. ~ country_plot, FUN = mean, data = M01)
M12.agg <- aggregate(. ~ country_plot, FUN = mean, data = M12)
M24.agg <- aggregate(. ~ country_plot, FUN = mean, data = M24)
M48.agg <- aggregate(. ~ country_plot, FUN = mean, data = M48)
OFH.agg <- aggregate(. ~ country_plot, FUN = mean, data = OFH)

soc.tog$M01.C <- M01.agg$SOC[match(soc.tog$country_plot, M01.agg$country_plot)]
soc.tog$M12.C <- M12.agg$SOC[match(soc.tog$country_plot, M12.agg$country_plot)]
soc.tog$M24.C <- M24.agg$SOC[match(soc.tog$country_plot, M24.agg$country_plot)]
soc.tog$M48.C <- M48.agg$SOC[match(soc.tog$country_plot, M48.agg$country_plot)]
soc.tog$OFH.C <- OFH.agg$OFH.SOC[match(soc.tog$country_plot, OFH.agg$country_plot)]

soc.tog$M01.bd <- M01.agg$BD[match(soc.tog$country_plot, M01.agg$country_plot)]
soc.tog$M12.bd <- M12.agg$BD[match(soc.tog$country_plot, M12.agg$country_plot)]
soc.tog$M24.bd <- M24.agg$BD[match(soc.tog$country_plot, M24.agg$country_plot)]
soc.tog$M48.bd <- M48.agg$BD[match(soc.tog$country_plot, M48.agg$country_plot)]

soc.tog$M01.coarse.f <- M01.agg$Coarse[match(soc.tog$country_plot, M01.agg$country_plot)]
soc.tog$M12.coarse.f <- M12.agg$Coarse[match(soc.tog$country_plot, M12.agg$country_plot)]
soc.tog$M24.coarse.f <- M24.agg$Coarse[match(soc.tog$country_plot, M24.agg$country_plot)]
soc.tog$M48.coarse.f <- M48.agg$Coarse[match(soc.tog$country_plot, M48.agg$country_plot)]

#Now make any value less than 0 NA
soc.tog[soc.tog < 0] <- NA

#Calculate mineral horizon soil C stock at respective cm depth
#Bulk density measurement is in kg/m3 and multiply by 0.001 to be g/cm3
#Remove the coarse fragment volume (%) by removing the fraction of density in BD that is coarse
#C measurements are g/kg and multiple by 0.1 to be a %
soc.tog$M01.C.stock <- (0.1*soc.tog$M01.C)*(soc.tog$M01.bd*0.001)*10
soc.tog$M01.C.stock.w.o.c <- (0.1*soc.tog$M01.C)*((soc.tog$M01.bd-(soc.tog$M01.bd*(soc.tog$M01.coarse.f*.01)))*0.001)*10

soc.tog$M12.C.stock <- (0.1*soc.tog$M12.C)*(soc.tog$M12.bd*0.001)*10
soc.tog$M12.C.stock.w.o.c <- (0.1*soc.tog$M12.C)*((soc.tog$M12.bd-(soc.tog$M12.bd*(soc.tog$M12.coarse.f*.01)))*0.001)*10

soc.tog$M24.C.stock <- (0.1*soc.tog$M24.C)*(soc.tog$M24.bd*0.001)*10
soc.tog$M24.C.stock.w.o.c <- (0.1*soc.tog$M24.C)*((soc.tog$M24.bd-(soc.tog$M24.bd*(soc.tog$M24.coarse.f*.01)))*0.001)*10

soc.tog$M48.C.stock <- (0.1*soc.tog$M48.C)*(soc.tog$M48.bd*0.001)*10
soc.tog$M48.C.stock.w.o.c <- (0.1*soc.tog$M48.C)*((soc.tog$M48.bd-(soc.tog$M48.bd*(soc.tog$M48.coarse.f*.01)))*0.001)*10

#Now add the SOC stock estimates to the tree growth and other ICP data.file data file
clay.df <- data.frame(clay = c(som.data$part_size_clay, fleck.data$part_size_clay),
                      country_plot = c(som.data$country_plot, fleck.data$country_plot),
                      depth = c(som.data$code_layer, fleck.data$code_layer))
clay.df <- clay.df[!grepl(-999999, clay.df$clay),] #Remove the missing fleck values represented as -999999
clay.df <- clay.df[!is.na(clay.df$clay), ]

#Now split by layer
clay.df.split <- split(clay.df, clay.df$depth)
soc.tog$clay.min <- clay.df.split$M01$clay[match(soc.tog$country_plot, clay.df.split$M01$country_plot)]

#Now add this to the ICP.dat dataframe
icp.dat <- merge(icp.dat, soc.tog, sep = 'country_plot')

#Add the soilgrids clay data (more complete)
soil.grids.data$lat_long <- paste(soil.grids.data$Latitude, soil.grids.data$Longitude, sep = '_')
icp.dat$lat_long <- paste(icp.dat$lat, icp.dat$long, sep = '_')
icp.dat <- merge(icp.dat, soil.grids.data, by = 'lat_long')
icp.dat$clay_0.5cm_mean <- icp.dat$clay_0.5cm_mean*0.1

#Add matching var for soil ph and then split by horizon
soil.ph.lab.meas$country_plot <- paste(soil.ph.lab.meas$Country, soil.ph.lab.meas$Plot, sep = '_')
soil.ph.lab.meas.split.hor <- split(soil.ph.lab.meas, soil.ph.lab.meas$Soil.Horizon)
icp.dat$M01.pH <- replace(icp.dat$M01.pH, which(icp.dat$M01.pH < 0), NA)
icp.dat$OFH.pH <- replace(icp.dat$Organic.OFH.pH, which(icp.dat$Organic.OFH.pH < 0), NA)
icp.dat$OFH.pH <- replace(icp.dat$OFH.pH, which(icp.dat$OFH.pH < 0), NA)

#Now let's coalese the two soil pH in mineral horizon columns to have one measure
icp.dat$soil.ph.lab.meas <- soil.ph.lab.meas.split.hor$Min$pH[match(icp.dat$country_plot, soil.ph.lab.meas.split.hor$Min$country_plot)]
icp.dat$Mineral.soil.pH <- coalesce(icp.dat$M01.pH, icp.dat$soil.ph.lab.meas)


##########################################################################################
####################################Pair datasets ########################################
##########################################################################################

#Get the list of sample names for each OTU table
org.envt <- data.frame(Sample = row.names(high.otu.sp$Org))
min.envt <- data.frame(Sample = row.names(high.otu.sp$Min))
row.names(org.envt) <- org.envt$Sample
row.names(min.envt) <- min.envt$Sample

#Add the ICP country and plot codes
org.envt$country <- envt.mat$Country[match(org.envt$Sample, envt.mat$File_name)]
org.envt$icp_plot <- envt.mat$Plot[match(org.envt$Sample, envt.mat$File_name)]
min.envt$country <- envt.mat$Country[match(min.envt$Sample, envt.mat$File_name)]
min.envt$icp_plot <- envt.mat$Plot[match(min.envt$Sample, envt.mat$File_name)]
org.envt$country_plot <- paste(org.envt$country, org.envt$icp_plot, sep = "_")
min.envt$country_plot <- paste(min.envt$country, min.envt$icp_plot, sep = "_")

#Now add the variables of interest from icp.dat

###
#Organic horizon predictors of interest

#MAT, MAP - climate
#SOC stocks, soil pH - edaphic
#N deposition - atmospheric
#Tree growth, tree type, stand age - stand properties
#Latitude - spatial

org.pred.vars <- c('country_plot', 'MAT', 'MAP', 'OFH.C', 'OFH.pH', 'N_dep_2019', 'mg_C_ha_yr', 'Tree_type', 'Stand.age', 'lat', 'Tree_richness')
org.envt <- join(org.envt, icp.dat[org.pred.vars], by = "country_plot")
org.envt <- org.envt[!duplicated(org.envt$Sample),] #Fix duplicates caused by merging...

###


###
#Mineral horizon predictors of interest

#MAT, MAP - climate
#SOC stocks, soil pH, clay content - edaphic
#N deposition - atmospheric
#Tree growth, tree type, stand age - stand properties
#Latitude - spatial

min.pred.vars <- c('country_plot', 'MAT', 'MAP', 'M01.C.stock.w.o.c', 'Mineral.soil.pH', 'clay.min','N_dep_2019', 'mg_C_ha_yr', 'Tree_type', 'Stand.age', 'lat', 'Tree_richness')
min.envt <- join(min.envt, icp.dat[min.pred.vars], by = "country_plot")
min.envt <- min.envt[!duplicated(min.envt$Sample),] #Fix duplicates caused by merging...

###
org.sp.df <- high.otu.sp$Org
min.sp.df <- high.otu.sp$Min
identical(org.envt$Sample, row.names(org.sp.df))
identical(min.envt$Sample, row.names(min.sp.df))


##########################################################################################
######################################Make models#########################################
##########################################################################################

#Distance-based redundancy analysis using bray-curtis dissimilarity

#Now make db-RDA model
org.db.model <- capscale(org.sp.df ~   
                       scale(MAT) + scale(MAP) + #climate
                       scale(log(OFH.C)) + scale(OFH.pH) + #edaphic
                       scale(N_dep_2019) + #N depo
                       scale(log(mg_C_ha_yr)) + Tree_type + scale(Stand.age) + scale(Tree_richness) + #stand level factors
                       scale(lat), #space
                       distance = 'bray', data = org.envt, na.action = "na.exclude")

summary(org.db.model) #CAP1 = 27.4%, CAP2 = 19.6%
anova(org.db.model)
RsquareAdj(org.db.model) #25.9%
anova(org.db.model, by="terms", permu = 999)

#Now make db-RDA model
min.db.model <- capscale(min.sp.df ~   
                           scale(MAT) + scale(MAP) + #climate
                           scale(log(M01.C.stock.w.o.c)) + scale(Mineral.soil.pH) + scale(clay.min) + #edaphic
                           scale(N_dep_2019) + #N depo
                           scale(log(mg_C_ha_yr)) + Tree_type + scale(Stand.age) + scale(Tree_richness) + #stand level factors
                           scale(lat), #space
                           distance = 'bray', data = min.envt, na.action = "na.exclude")

summary(min.db.model) #CAP1 = 25.6, CAP2 = 13.7
anova(min.db.model)
RsquareAdj(min.db.model) #22.8
anova(min.db.model, by="terms", permu = 999)


##############PCoA/NMDS and vector fitting for more exploratory analyses#############
org.pcoa <- pcoa(vegdist(org.sp.df))
min.pcoa <- pcoa(vegdist(min.sp.df))
org.mds <- metaMDS(org.sp.df, distance = 'bray')
min.mds <- metaMDS(min.sp.df, distnace = 'bray')

#Vector fitting with PCoA
pcoa.org.envt.fit <- envfit(org.pcoa$vectors[,1:2] ~  scale(MAT) + scale(MAP) + #climate
                         scale(log(OFH.C)) + scale(OFH.pH) + #edaphic
                         scale(N_dep_2019) + #N depo
                         scale(log(mg_C_ha_yr)) + Tree_type + scale(Stand.age) + scale(Tree_richness) + #stand level factors
                         scale(lat), #space
                         data = org.envt, na.rm = TRUE)

pcoa.min.envt.fit <- envfit(min.pcoa$vectors[,1:2] ~  scale(MAT) + scale(MAP) + #climate
                         scale(log(M01.C.stock.w.o.c)) + scale(Mineral.soil.pH) + scale(clay.min) + #edaphic
                         scale(N_dep_2019) + #N depo
                         scale(log(mg_C_ha_yr)) + Tree_type + scale(Stand.age) + scale(Tree_richness) + #stand level factors
                         scale(lat), #space
                         data = min.envt, na.rm = TRUE)

#Vector fitting with NMDS
nmds.org.envt.fit <- envfit(org.mds$points[,1:2] ~  scale(MAT) + scale(MAP) + #climate
                         scale(log(OFH.C)) + scale(OFH.pH) + #edaphic
                         scale(N_dep_2019) + #N depo
                         scale(log(mg_C_ha_yr)) + Tree_type + scale(Stand.age) + #stand level factors
                         scale(lat), #space
                         data = org.envt, na.rm = TRUE)

nmds.min.envt.fit <- envfit(min.mds$points[,1:2] ~  scale(MAT) + scale(MAP) + #climate
          scale(log(M01.C.stock.w.o.c)) + scale(Mineral.soil.pH) + scale(clay.min) + #edaphic
          scale(N_dep_2019) + #N depo
          scale(log(mg_C_ha_yr)) + Tree_type + scale(Stand.age) + #stand level factors
          scale(lat), #space
          data = min.envt, na.rm = TRUE)



##########################################################################################
##################################Make visualizations#####################################
##########################################################################################

#Start with the PCoA results since this is most translatable to subsequent analyses

#Organic horizon
org.pcoa.plot <- data.frame(org.pcoa$vectors[,1:2])
org.pcoa.plot <- cbind(org.pcoa.plot, org.envt)
org.pcoa.envt.fit.results <- data.frame(scores(pcoa.org.envt.fit, c("vectors")))
org.pcoa.envt.fit.results$p.value <- pcoa.org.envt.fit$vectors$pvals                                      
org.pcoa.envt.fit.results$r.value <- pcoa.org.envt.fit$vectors$r                                      
org.pcoa.envt.fit.results$labels <- c("MAT", "MAP", "C stocks", "pH",
                                      "N dep.", "Tree growth", "Forest age",
                                      "Space")
org.pcoa.envt.fit.results$significance <- ifelse(org.pcoa.envt.fit.results$p.value < 0.05, "solid", "dashed")
org.pcoa.envt.fit.results$sig2 <- org.pcoa.envt.fit.results$significance
org.pcoa.envt.fit.results$sig2 <- gsub("dashed", "NS", org.pcoa.envt.fit.results$sig2)
org.pcoa.envt.fit.results$sig2 <- gsub("solid", "Sig.", org.pcoa.envt.fit.results$sig2)

org.hor.fungi <- ggplot(org.pcoa.plot[!is.na(org.pcoa.plot$Tree_type), ], aes(x = Axis.1, y = Axis.2, color = Tree_type)) +
         geom_point()+
         scale_color_manual(values = c("#d23cf7", "#9dd80b"))+ #Needle, broad
         theme_classic()+
         labs(y = "PCoA2", x = "PCoA1", color = "") +
         annotate("segment", x = rep(0, length(org.pcoa.envt.fit.results$Axis.1)), 
                  xend = 0.45*org.pcoa.envt.fit.results$Axis.1, 
                  y = rep(0, length(org.pcoa.envt.fit.results$Axis.1)), 
                  yend = 0.45*org.pcoa.envt.fit.results$Axis.2, 
                  linetype = org.pcoa.envt.fit.results$significance,
                  colour = "black", size=0.5, arrow=arrow(length = unit(.2,"cm"))) #+
         geom_text_repel(data = org.pcoa.envt.fit.results, aes(x = 0.5*Axis.1, 
                  y = 0.5*Axis.2, 
                  label = labels),
                  colour = "black")

#Mineral horizon
min.pcoa.plot <- data.frame(min.pcoa$vectors[,1:2])
min.pcoa.plot <- cbind(min.pcoa.plot, min.envt)
min.pcoa.envt.fit.results <- data.frame(scores(pcoa.min.envt.fit, c("vectors")))
min.pcoa.envt.fit.results$p.value <- pcoa.min.envt.fit$vectors$pvals                                      
min.pcoa.envt.fit.results$r.value <- pcoa.min.envt.fit$vectors$r                                      
min.pcoa.envt.fit.results$labels <- c("MAT", "MAP", "C stocks", "pH", "clay",
                                      "N dep.", "Tree growth", "Forest age",
                                      "Space")
min.pcoa.envt.fit.results$significance <- ifelse(min.pcoa.envt.fit.results$p.value < 0.05, "solid", "dashed")
min.pcoa.envt.fit.results$sig2 <- min.pcoa.envt.fit.results$significance
min.pcoa.envt.fit.results$sig2 <- gsub("dashed", "NS", min.pcoa.envt.fit.results$sig2)
min.pcoa.envt.fit.results$sig2 <- gsub("solid", "Sig.", min.pcoa.envt.fit.results$sig2)


min.hor.fungi <- ggplot(min.pcoa.plot[!is.na(min.pcoa.plot$Tree_type), ], aes(x = Axis.1, y = Axis.2, color = Tree_type)) +
  geom_point()+
  scale_color_manual(values = c("#d23cf7", "#9dd80b"))+ #Needle, broad
  theme_classic()+
  labs(y = "PCoA2", x = "PCoA1", color = "") +
  annotate("segment", x = rep(0, length(min.pcoa.envt.fit.results$Axis.1)), 
           xend = 0.45*min.pcoa.envt.fit.results$Axis.1, 
           y = rep(0, length(min.pcoa.envt.fit.results$Axis.1)), 
           yend = 0.45*min.pcoa.envt.fit.results$Axis.2, 
           linetype = min.pcoa.envt.fit.results$significance,
           colour = "black", linewidth=0.5, arrow=arrow(length = unit(.2,"cm"))) #+
   geom_text_repel(data = min.pcoa.envt.fit.results, aes(x = 0.5*Axis.1, 
            y = 0.5*Axis.2, 
            label = labels),
            colour = "black")

plot_grid(org.hor.fungi, min.hor.fungi, ncol = 1)

#Plot veg.dist r2 values, but first add the tree type

#Organic horizon
org.pcoa.envt.fit.results.treetype <- data.frame(Axis.1 = NA, Axis.2 = NA, 
                                                 p.value = pcoa.org.envt.fit$factors$pvals,
                                                 r.value = pcoa.org.envt.fit$factors$r,
                                                 labels = "Tree type",
                                                 significance = NA,
                                                 sig2 = "Sig.")
org.pcoa.envt.fit.results <- rbind(org.pcoa.envt.fit.results, org.pcoa.envt.fit.results.treetype)

#Mineral horizon
min.pcoa.envt.fit.results.treetype <- data.frame(Axis.1 = NA, Axis.2 = NA, 
                                                 p.value = pcoa.min.envt.fit$factors$pvals,
                                                 r.value = pcoa.min.envt.fit$factors$r,
                                                 labels = "Tree type",
                                                 significance = NA,
                                                 sig2 = "Sig.")
min.pcoa.envt.fit.results <- rbind(min.pcoa.envt.fit.results, min.pcoa.envt.fit.results.treetype)


#Now combine the organic and mineral and plot together to get an overall impression
org.pcoa.envt.fit.results$horizon <- rep("Organic", length(org.pcoa.envt.fit.results$Axis.1))
min.pcoa.envt.fit.results$horizon <- rep("Mineral", length(min.pcoa.envt.fit.results$Axis.1))
pcoa.envt.fit.results <- rbind(org.pcoa.envt.fit.results, min.pcoa.envt.fit.results)
pcoa.envt.fit.results$labels <- factor(pcoa.envt.fit.results$labels, levels = c("MAT", "MAP", "N dep.","Space",
                                                                                "pH",
                                                  "C stocks", "clay", "Tree type", "Forest age",
                                                  "Tree growth")) 

veg.dist.both <- ggplot(pcoa.envt.fit.results, aes(x = labels, y = round(r.value, 2), fill = horizon))+
  geom_bar(stat = 'identity', color = "black", show.legend = FALSE, width = 0.5)+
  theme_classic()+
  coord_flip()+
  ylim(c(0,1))+
#  theme(axis.text.y = element_blank())+
  scale_fill_manual(values = c("white", "grey"))+
  labs(y = expression(italic(r)^{2}), x = "", fill = "")


#Now plot beside the bacterial r2 plot...must run that script before this leg 
#of this script will work
bottom <- plot_grid(veg.dist.both, veg.dist.both.bact, axes = "vh")

